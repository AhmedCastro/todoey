//
//  Item.swift
//  Todoey
//
//  Created by Ahmed Castro on 12/15/18.
//  Copyright © 2018 Cleveri. All rights reserved.
//

import Foundation

class Item: Codable {
    
    var title = ""
    var done = false
    
}
